const college=require('../Model/College');
const student=require('../Model/Students');
const marks=require('../Model/Marks');
const mongoose=require('mongoose');

//CREATE 
const collegeCrt=async(req,res)=>{
    try{
        console.log(req.body);
        const data = await college.create(req.body);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
   
}
const marksCrt=async(req,res)=>{
    try{
        const studentId=req.params.id;
        const data = await marks.create({studentId:studentId,...req.body});
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
    
}
const studentCrt=async(req,res)=>{
    try{
        const collegeId=req.params.id;
        const data = await student.create({collegeId:collegeId,...req.body});
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
   
}

//READ ONE DATA USING ID
const getOneMarks=async (req,res)=>{
    try{
        const _id=req.params.id;
        
        const data=await marks.findOne({"_id":_id}).populate({path:'studentId',populate:{path:'collegeId'}});
        //const data=await marks.findOne({"_id":_id}).populate('studentId').select("Subject Marks studentId");
        if(data==null){
            throw new Error("Marks is not found in database");
        }
        console.log(_id);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}

const getOneStudent=async (req,res)=>{
    try{
        const _id=req.params.id;
        const data=await student.findOne({"_id":_id}).populate('collegeId');
        if(data==null){
            throw new Error("Marks is not found in database");
        }
        console.log(_id);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}

const getOneCollege=async (req,res)=>{
    try{
        const _id=req.params.id;
        const data=await college.findOne({"_id":_id})
        if(data==null){
            throw new Error("Marks is not found in database");
        }
        console.log(_id);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}

//READ ALL DATA
const getAllCollege=async (req,res)=>{
    try{
        const data=await college.find();
        if(data==null){
            throw new Error("college is not found in database");
        }
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}
//get all marks data 
// const getAllMarks=async (req,res)=>{
//     try{
//         // ********** Select **************
//         const data=await marks.find().select("Subject studentId");
//         if(data==null){
//             throw new Error("Marks is not found in database");
//         }
//        // console.log(studentId);
//         return res.status(200).send(data);
//     }
//     catch(err){
//         console.log(err.message||err);
//         return res.status(400).send(err);
//     }
// }
const getAllMarks = async (req, res) => {
    try {
        const { page,size} = req.query;
        const limit = size|| 3;
        const skip = (page-1)*limit|| 0;

        const data = await marks.aggregate([
            {
                $skip: skip
            }
            ,{
                $limit: limit
            }
            // ,{
            //     $project: {
            //         Subject: 1,
            //         studentId: 1
            //     }
            // }
        ]);

        if (data.length === 0) {
            throw new Error("Marks are not found in the database");
        }

        return res.status(200).send(data);
    } catch (err) {
        console.error(err.message || err);
        return res.status(400).send(err);
    }
}

//get lookup marks data
const getMarksWithLookup = async (req, res) => {
    try {
        const data = await marks.aggregate([
            {
                $lookup: { // Lookup to join with the students collection
                    from: 'students',
                    localField: 'studentId',
                    foreignField: '_id',
                    as: 'student'
                }
            },{ $unwind: '$student' } 
            ,{
                $project: { 
                    Subject: 1,
                    Marks: 1,
                    'student.fName': 1,
                    'student.lName': 1
                }
            }
            ,{
                $sort:{"Marks":-1}
            }
        ]);

        console.log("Lookup with Students collection");
        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err);
    }
}
//get matched marks data
const getAllMarksMatched = async (req, res) => {
    try {
        const studentId = req.params.id;
        console.log(studentId);
        const data = await marks.aggregate([
            {
                $match: {
                    studentId:new  mongoose.Types.ObjectId(studentId)
                }
            }
            // ,{
            //     $project: {
            //         Subject: 1,
            //         studentId: 1
            //     }
            // }
            
        ]);

        if (data.length === 0) {
            throw new Error("No marks found for the given student ID");
        }

        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err);
    }
}
// *********** read all marks using student id ************
const getAllMarksStd=async (req,res)=>{
    try{
        const studentId=req.params.id;
        const data=await marks.find({"studentId":studentId})
        if(data==null){
            throw new Error("Marks is not found in database");
        }
        console.log(studentId);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}

const getAllStudents=async (req,res)=>{
    try{
        const data=await student.find().select("fName lName");
        if(data==null){
            throw new Error("students is not found in database");
        }
       // console.log(studentId);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}
////  sorting of students//
const getStudentsSorted = async (req, res) => {
    try {
        const data = await student.aggregate([
            {
                $sort: {
                    "lName": -1
                }
            }
        ]);

        console.log("Sorted by CollegeId");
        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err);
    }
}
//********* Get all student data using college ID */
const getAllStudentsCllg=async (req,res)=>{
    try{
        const collegeId=req.params.id;
        const data=await student.find({"collegeId":collegeId})
        if(data==null){
            throw new Error("Marks is not found in database");
        }
        console.log(collegeId);
        return res.status(200).send(data);
    }
    catch(err){
        console.log(err.message||err);
        return res.status(400).send(err);
    }
}

////////   Update  USING _ID   ////////

const updateMarks=async (req,res)=>{
    try{

        const id=req.params.id;
        const result = await marks.findByIdAndUpdate(id,req.body,  { new: true });
        // console.log(req.body);
        console.log("Subject-marks section updated for _id:", id);
        console.log(result);
        res.status(200).send(result);
    }
    catch(err){
        console.log(err);
        res.status(400).send(err.message||err);
    } 
}
const updateStudents=async (req,res)=>{
    try{

        const id=req.params.id;
        const result = await student.findByIdAndUpdate(id,req.body,  { new: true });
        // console.log(req.body);
        console.log("Subject-marks section updated for _id:", id);
        console.log(result);
        res.status(200).send(result);
    }
    catch(err){
        console.log(err);
        res.status(400).send(err.message||err);
    } 
}
const updateCollege=async (req,res)=>{
    try{

        const id=req.params.id;
        const result = await college.findByIdAndUpdate(id,req.body,  { new: true });
        // console.log(req.body);
        console.log("Subject-marks section updated for _id:", id);
        console.log(result);
        res.status(200).send(result);
    }
    catch(err){
        console.log(err);
        res.status(400).send(err.message||err);
    } 
}


module.exports={collegeCrt,marksCrt,studentCrt,getOneMarks,getOneStudent,getOneCollege,getAllMarks,getAllStudents,getAllCollege,updateCollege,updateMarks,updateStudents,getAllStudentsCllg,getAllMarksStd,getAllMarksMatched,getMarksWithLookup,getStudentsSorted};

